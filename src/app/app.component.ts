import { Component, OnInit } from '@angular/core';
import { SocketService } from './services/socketService';

@Component({
    selector: 'as-main-app',
    templateUrl: 'app/app.html'
})
export class AppComponent implements OnInit {
  constructor(private socketService: SocketService) { }

  ngOnInit() {
    this.socketService.socketConnect();
  }


}
