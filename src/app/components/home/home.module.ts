import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HomeComponent } from './home.component';
import { HelpersModule } from './../../helpers/helpers.module';
import { HomeService } from './../../services/homeService';

import { homeRouting } from './home.routes';

@NgModule({
    declarations: [
        HomeComponent,
    ],
    imports: [
      HelpersModule,
      BrowserModule,
      FormsModule,
      homeRouting
    ],
    exports: [
        HomeComponent
    ],
    providers: [HomeService]
})
export class HomeModule {
}
