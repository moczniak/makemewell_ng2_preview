import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HomeService } from './../../services/homeService';
import { AuthService } from './../../services/authService';

import { LoginForm } from './../../forms/loginForm';
import { RegisterForm } from './../../forms/registerForm';
import { ForgetNewPasswordForm } from './../../forms/forgetNewPasswordForm';


@Component({
    selector: 'as-home',
    templateUrl: 'app/components/home/home.html'
})
export class HomeComponent implements OnInit, OnDestroy {

    loginFormModel = new LoginForm('', '');
    successLoginMessage: string = '';
    successLoginShow: number = 0;
    errorLoginMessage: string = '';
    errorLoginShow: number = 0;

    registerFormModel = new RegisterForm('', '', '', '', false);
    successRegisterMessage: string = '';
    successRegisterShow: number = 0;
    errorRegisterMessage: string = '';
    errorRegisterShow: number = 0;

    private subs: Array<any> = [];

    forgetPasswordFormShow: boolean = false;
    forgetPasswordEmail: string;
    forgetPasswordToken: string;
    forgetNewPasswordFormModel = new ForgetNewPasswordForm('', '', '');
    logedIn: boolean = false;
    t1_uid: number = 0;

    constructor(private homeService: HomeService, private authService: AuthService, private route: ActivatedRoute) { }

    ngOnInit() {
      const sub = this.route.params.subscribe(params => {
        if (params['token']) {
          this.forgetPasswordFormShow = true;
          this.forgetPasswordToken = params['token'];
        }
       });
      this.subs.push(sub);
      this.checkIfLoged();
    }

    private checkIfLoged() {
      const token = this.authService.getAuthToken();
      if (token !== '' && token !== null) {
        const sub = this.homeService.checkToken(token)
                      .subscribe(
                        success => {
                          this.t1_uid = success.data.t1_uid;
                          this.authService.setAuthStatus(true);
                          this.logedIn = true;
                        },
                        error => this.authService.removeAuthToken()
                      );
        this.subs.push(sub);
      }
    }

    sendLoginForm() {
      const sub = this.homeService.sendLoginForm(this.loginFormModel)
                    .subscribe(
                      success => {
                        this.authService.setAuthToken(success.data.authToken);
                        this.logedIn = true;
                        this.loginFormModel = new LoginForm('', '');
                        this.t1_uid = success.data.t1_uid;
                      },
                      error => this.showLoginError(error)
                    );
      this.subs.push(sub);
    }

    createWorld(world: number) {
      const sub = this.homeService.createWorld(world)
                    .subscribe(
                      success => {
                          this.authService.setAuthToken(success.data.authToken);
                          this.t1_uid = success.data.t1_uid;
                      },
                      error => console.log(error)
                    );
      this.subs.push(sub);
    }

    logout() {
      this.authService.removeAuthToken();
      this.logedIn = false;
      this.t1_uid = 0;
    }

    sendForgetPasswordForm() {
      const sub = this.homeService.sendForgetPasswordEmail(this.forgetPasswordEmail)
                    .subscribe(
                      success => {
                        this.forgetPasswordEmail = '';
                        this.showLoginSuccess('Sprawdź swojego emaila');
                      },
                      error => this.showLoginError(error)
                    );
      this.subs.push(sub);
    }

    sendForgetNewPasswordForm() {
      this.forgetNewPasswordFormModel.token = this.forgetPasswordToken;
      const sub = this.homeService.sendForgetNewPasswordForm(this.forgetNewPasswordFormModel)
                    .subscribe(
                      success => {
                        this.forgetNewPasswordFormModel = new ForgetNewPasswordForm('', '', '');
                        this.showLoginSuccess('Możesz się zalogować');
                      },
                      error => this.showLoginError(error)
                    );
      this.subs.push(sub);
    }

    sendRegisterForm() {
      const sub = this.homeService.sendRegisterForm(this.registerFormModel)
                    .subscribe(
                      success => {
                        this.showRegisterSuccess('Konto zostało załozone, możesz się kurwić');
                        this.registerFormModel = new RegisterForm('', '', '', '', false);
                      },
                      error => this.showRegisterError(error)
                    );
      this.subs.push(sub);
    }




    private showLoginSuccess(message: string) {
      this.successLoginMessage = message;
      this.successLoginShow++;
    }

    private showLoginError(message: any) {
      this.errorLoginMessage = message;
      this.errorLoginShow++;
    }

    private showRegisterSuccess(message: string) {
      this.successRegisterMessage = message;
      this.successRegisterShow++;
    }
    private showRegisterError(message: any) {
      this.errorRegisterMessage = message;
      this.errorRegisterShow++;
    }
    ngOnDestroy() {
      this.subs.map(sub => {
        sub.unsubscribe();
      });
    }

}
