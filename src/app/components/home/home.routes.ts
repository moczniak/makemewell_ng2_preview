import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

export const HomeRoutes: Routes = [
  { path: '',  component: HomeComponent },
  {path: 'forgetPassword/:token', component: HomeComponent }
];

export const homeRouting = RouterModule.forChild(HomeRoutes);
