import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { GameComponent } from './game.component';
import { BarPanelComponent } from './panels/barPanel.component';
import { InfoPanelComponent } from './panels/infoPanel.component';
import { ActionPanelComponent } from './panels/actionPanel.component';
import { HealthPanelComponent } from './panels/healthPanel.component';
import { ErrorGamePopupComponent } from './panels/errorGamePopup.component';
import { SuccessGamePopupComponent } from './panels/successGamePopup.component';

import { HelpersModule } from './../../helpers/helpers.module';

import { SheduleModule } from './house/shedule.module';
import { TrainingModule } from './house/training.module';
import { AchievementModule } from './house/achievement.module';


import { RaportsModule } from './raports/raports.module';
import { BarModule } from './bar/bar.module';

import { ErrorService } from './../../services/inGame/errorService';
import { SuccessService } from './../../services/inGame/successService';
import { CoreService } from './../../services/inGame/coreService';
import { PanelsService } from './../../services/inGame/panelsService';
import { ChatService } from './../../services/inGame/chatService';

import { SharedModule } from './shared.module';

import { gameRouting } from './game.routes';

@NgModule({
    declarations: [
        GameComponent,
        BarPanelComponent,
        InfoPanelComponent,
        ActionPanelComponent,
        HealthPanelComponent,
        ErrorGamePopupComponent,
        SuccessGamePopupComponent
    ],
    imports: [
      HelpersModule,
      BrowserModule,
      FormsModule,
      gameRouting,

      TrainingModule,
      SheduleModule,
      AchievementModule,

      RaportsModule,
      BarModule,
      SharedModule
    ],
    exports: [
        GameComponent
    ],
    providers: [ErrorService, SuccessService, CoreService, PanelsService, ChatService]
})
export class GameModule {
}
