import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CoreService } from './../../services/inGame/coreService';
import { PanelsService } from './../../services/inGame/panelsService';
import { SocketService } from './../../services/socketService';
declare var isMobile: any;


@Component({
  templateUrl: 'app/components/inGame/game.html'
})

export class GameComponent implements OnInit, OnDestroy {

  private subs = [];

  isMobileStatus: boolean = false;
  isMenuOpen: boolean = false;

  private currentUrl: string;

  isNewRaport: boolean = false;

  constructor(private router: Router, private coreService: CoreService, private panelsService: PanelsService,
              private socketService: SocketService) { }

  ngOnInit() {
    const sub = this.router.events
                   .subscribe(
                     event => {
                       if (event instanceof NavigationEnd) {
                         this.currentUrl = event.url;
                         if (event.url !== '/game/shedule') {
                           this.updateAccount();
                         }
                         this.toggleMenu();
                       }
                     }
                   );
    this.subs.push(sub);
    this.socketService.joinEventRoom();

    this.watchNewRaport();

    this.isMobileStatus = isMobile.any ? true : false;
    this.isMenuOpen = this.isMobileStatus;
  }

  private watchNewRaport() {
    const sub = this.socketService.$newRaport
                    .subscribe(
                      value => {
                        if (this.currentUrl !== '/game/raports') {
                          this.isNewRaport = true;
                        }
                      }
                    );
    this.subs.push(sub);
  }

  private updateAccount() {
    const sub = this.coreService.updateAccount()
                    .subscribe(
                      success => {
                        this.coreService.vipStatus = success.data.vipStatus;
                        this.coreService.vipFactors = success.data.vipFactors;
                        this.panelsService.setPanelData(success);
                      }
                    );
    this.subs.push(sub);
  }

  toggleMenu() {
    if (this.isMobileStatus) {
      this.isMenuOpen = this.isMenuOpen ? false : true;
    }
  }
  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
    this.socketService.leaveEventRoom();
  }



}
