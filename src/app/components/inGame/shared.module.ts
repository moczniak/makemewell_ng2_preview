import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { TimeStrPipe } from './../../pipes/timeStr.pipe';
import { NumberFormatPipe } from './../../pipes/numberFormat.pipe';
import { SecondsToStrPipe } from './../../pipes/secondsToStr.pipe';

import { PaginateComponent } from '../../helpers/paginate.component';
import { ChatComponent } from '../../helpers/chat.component';

import { PerfectScrollbarDirective } from './../../directives/perfectScrollbar.directive';

@NgModule({
    declarations: [
        PaginateComponent,
        ChatComponent,
        NumberFormatPipe,
        SecondsToStrPipe,
        TimeStrPipe,

        PerfectScrollbarDirective
    ],
    imports: [
      BrowserModule,
      FormsModule
    ],
    exports: [
      PaginateComponent,
      ChatComponent,
      TimeStrPipe,
      NumberFormatPipe,
      SecondsToStrPipe,

      PerfectScrollbarDirective
    ],
})
export class SharedModule {
}
