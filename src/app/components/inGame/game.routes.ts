import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { SheduleComponent } from './house/shedule.component';
import { TrainingComponent } from './house/training.component';
import { AchievementComponent } from './house/achievement.component';
import { RaportsComponent } from './raports/raports.component';
import { BarComponent } from './bar/bar.component';
import { AuthService } from './../../services/authService';

export const GameRoutes: Routes = [
  {
    path: 'game',
    component: GameComponent,
    children: [
        {path: 'shedule', component: SheduleComponent, canActivate: [AuthService]},
        {path: 'training', component: TrainingComponent, canActivate: [AuthService]},
        {path: 'achievements', component: AchievementComponent, canActivate: [AuthService]},

        {path: 'raports', component: RaportsComponent, canActivate: [AuthService]},
        {path: 'bar', component: BarComponent, canActivate: [AuthService]}
    ]
  }
];


export const gameRouting = RouterModule.forChild(GameRoutes);
