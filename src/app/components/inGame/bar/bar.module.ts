import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { BarComponent } from './bar.component';

import { BarService } from './../../../services/inGame/barService';


import { SharedModule } from './../shared.module';

@NgModule({
    declarations: [
      BarComponent
    ],
    imports: [
      BrowserModule,
      FormsModule,
      SharedModule
    ],
    exports: [
      BarComponent
    ],
    providers: [BarService],
})
export class BarModule {
}
