import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorService } from './../../../services/inGame/errorService';
import { BarService } from './../../../services/inGame/barService';
import { UserInfo, SlotsInfo, SlotsWinnerReward } from './../../../responses/inGame/barResponse';
import { PanelsService } from './../../../services/inGame/panelsService';

@Component({
  templateUrl: 'app/components/inGame/bar/bar.html',
  styleUrls: ['app/components/inGame/bar/bar.css']
})

export class BarComponent implements OnInit, OnDestroy {

  private subs = [];

  barUserInfo: UserInfo;
  barSlotsInfo: SlotsInfo;

  isPlaying: boolean = false;
  slotsColumn: Array<Array<string>>;
  slotsWinnerReward: SlotsWinnerReward;
  slotsWinnerRewardShow: boolean = false;
  multiplier: number = 1;
  slotsPoints: number = 0;

  constructor(private barService: BarService, private errorService: ErrorService, private panelsService: PanelsService) { }

  ngOnInit() {
    this.getBarUserInfo();
    this.getBarSlotsInfo();
  }

  private getBarUserInfo() {
    const sub = this.barService.getUserInfo()
                    .subscribe(
                      success => {
                        this.barUserInfo = success.data;
                      }
                    );
    this.subs.push(sub);
  }

  private getBarSlotsInfo() {
    const sub = this.barService.getSlotsInfo()
                    .subscribe(
                      success => {
                        console.log(success);
                        this.barSlotsInfo = success.data;
                        this.slotsColumn = success.data.columns;
                        this.slotsPoints = success.data.slotsPoints;
                      }
                    );
    this.subs.push(sub);
  }

  playSlots() {
    if (this.isPlaying === false) {
      this.isPlaying = true;
      this.slotsWinnerRewardShow = false;
      const sub = this.barService.playSlots(this.multiplier)
                      .subscribe(
                        success => {
                          console.log(success);
                          this.slotsColumn = success.data.columns;
                          this.slotsWinnerReward = success.data.reward;
                          setTimeout(() => {
                            this.isPlaying = false;
                            this.slotsWinnerRewardShow = true;
                            this.slotsPoints = success.data.slotsPoints;
                            this.panelsService.setPanelData(success);
                            setTimeout(() => {
                              this.slotsWinnerRewardShow = false;

                            }, 2000);
                          }, 2000);
                        },
                        error => this.errorService.setError(error)
                      );
      this.subs.push(sub);
    }
  }

  buySlotsPoints(amount: number) {
    // TODO: zrobic kupowanie vip
  }


  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }


}
