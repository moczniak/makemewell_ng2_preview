import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorService } from './../../../services/inGame/errorService';

@Component({
  selector: 'as-game-error',
  template: `
  <div class="alert alert-danger" [hidden]="!show">
    {{message}}
  </div>
  `
})

export class ErrorGamePopupComponent implements OnInit, OnDestroy {

  private errorSub: any;
  private timeout: any;
  show: boolean = false;
  message: string;

  constructor(private errorService: ErrorService) { }

  ngOnInit() {
    this.watchErrors();
  }

  private watchErrors() {
    this.errorSub = this.errorService.$errorMessage
                        .subscribe(
                          value => this.setErrorMessage(value)
                        );
  }

  private setErrorMessage(message: string) {
    this.show = true;
    this.message = message;
    this.timeout = setTimeout(() => {
      this.show = false;
      this.message = '';
    }, 5000);
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }

}
