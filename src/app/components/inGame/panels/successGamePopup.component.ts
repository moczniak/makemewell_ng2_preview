import { Component, OnInit, OnDestroy } from '@angular/core';
import { SuccessService } from './../../../services/inGame/successService';

@Component({
  selector: 'as-game-success',
  template: `
  <div class="alert alert-success" [hidden]="!show">
    {{message}}
  </div>
  `
})

export class SuccessGamePopupComponent implements OnInit, OnDestroy {

  private successSub: any;
  private timeout: any;
  show: boolean = false;
  message: string;

  constructor(private successService: SuccessService) { }

  ngOnInit() {
    this.watchSuccess();
  }

  private watchSuccess() {
    this.successSub = this.successService.$successMessage
                        .subscribe(
                          value => this.setSuccessMessage(value)
                        );
  }

  private setSuccessMessage(message: string) {
    this.show = true;
    this.message = message;
    this.timeout = setTimeout(() => {
      this.show = false;
      this.message = '';
    }, 5000);
  }

  ngOnDestroy() {
    this.successSub.unsubscribe();
  }

}
