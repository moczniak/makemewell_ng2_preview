import { Component, OnInit, OnDestroy } from '@angular/core';
import { PanelsService } from './../../../services/inGame/panelsService';
import { UserInfo } from './../../../responses/inGame/coreResponse';

@Component({
  selector: 'as-infoPanel',
  templateUrl: 'app/components/inGame/panels/infoPanel.html',
  styles: [`
  .table-responsive > .table {
    background-color: transparent;
  }
  .table {
    font-size: small;
  }
  `]
})


export class InfoPanelComponent implements OnInit, OnDestroy {
  private subs = [];
  isMobile: boolean = false;
  isBodyShow: boolean = false;

  userInfo: UserInfo;

  moneyChanged: boolean = false;
  prestigeChanged: boolean = false;
  fameChanged: boolean = false;
  vipChanged: boolean = false;

  constructor(private panelsService: PanelsService) { }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 768 ? true : false;
    this.isBodyShow = this.isMobile ? false : true;
    this.watchInfoData();
  }

  showBody() {
    if (this.isMobile) {
      this.isBodyShow = this.isBodyShow ? false : true;
    }
  }

  private watchInfoData() {
    const sub = this.panelsService.$userData
                   .subscribe(
                     success => {
                       this.checkChanges(success);
                       this.userInfo = success;
                     }
                   );
    this.subs.push(sub);
  }

  private checkChanges(newValues: UserInfo) {
    if (this.userInfo) {
      if (this.userInfo.money !== newValues.money) {
        this.moneyChanged = false;
        setTimeout(() => this.moneyChanged = true, 50);
      }
      if (this.userInfo.prestige !== newValues.prestige) {
        this.prestigeChanged = false;
        setTimeout(() => this.prestigeChanged = true, 50);
      }
      if (this.userInfo.fame !== newValues.fame) {
        this.fameChanged = false;
        setTimeout(() => this.fameChanged = true, 50);
      }
      if (this.userInfo.vip !== newValues.vip) {
        this.vipChanged = false;
        setTimeout(() => this.vipChanged = true, 50);
      }
    }
  }

  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }
}
