import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'as-barPanel',
  templateUrl: 'app/components/inGame/panels/barPanel.html'
})


export class BarPanelComponent implements OnInit {

  isMobile: boolean = false;
  isBodyShow: boolean = false;

  constructor( ) { }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 768 ? true : false;
    this.isBodyShow = this.isMobile ? false : true;
  }

  showBody() {
    if (this.isMobile) {
      this.isBodyShow = this.isBodyShow ? false : true;
    }
  }

}
