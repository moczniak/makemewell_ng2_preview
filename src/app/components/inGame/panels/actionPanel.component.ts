import { Component, OnInit, OnDestroy } from '@angular/core';
import { PanelsService } from './../../../services/inGame/panelsService';
import { CoreService } from './../../../services/inGame/coreService';
import { ErrorService } from './../../../services/inGame/errorService';
import * as moment from 'moment';

import { Shedule } from './../../../responses/inGame/coreResponse';

@Component({
  selector: 'as-actionPanel',
  templateUrl: 'app/components/inGame/panels/actionPanel.html'
})

// TODO: gif czy progressBar???

export class ActionPanelComponent implements OnInit, OnDestroy {

  private subs = [];
  panelTitle: string = 'Ruchanko?';
  endJobStamp: number;
  job: boolean = false;
  jobType: string;
  jobProgressBar: boolean = false;
  progressBarEnd: boolean = true;
  progressBarEndCounter: number = 0;

  barPercent: number = 100;
  secondsCounting: number = 0;

  allSeconds: number;
  firstSetData: boolean = false;
  lastCalledUpdate: any;

  jobFast: boolean = false;
  jobMid: boolean = false;
  jobLong: boolean = false;

  rideBack: boolean = false;

  mainInterval: any;

  sheduleData: Shedule;

  constructor(private panelsService: PanelsService, private coreService: CoreService, private errorService: ErrorService) { }

  ngOnInit() {
    this.watchActionData();
    setTimeout(() => {
        if (this.firstSetData === false) {
          this.updateAccount();
        }
    }, 500);
    this.watchForBrowserTabChanges();
    this.mainIntervalLoop();
  }

  whoreJobCall(option: number) {
    const sub = this.panelsService.setActiveSheduleUpdateOption(option)
                    .subscribe(
                      success => {
                        this.updateAccount();
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }
  private updateAccount() {
    const sub = this.coreService.updateAccount()
                    .subscribe(
                      success => {
                        this.panelsService.setPanelData(success);
                      }
                    );
    this.subs.push(sub);
  }

  private watchActionData() {
    const sub = this.panelsService.$sheduleData
                   .subscribe(
                     success => {
                       console.log(success);
                       this.setPanelTitle(success.typ);
                       this.endJobStamp = success.endStamp;
                       if (this.jobProgressBar === false && this.firstSetData === true) {
                         this.panelsService.setLoadLocationCounter(this.progressBarEndCounter++);
                       }
                       this.sheduleData = success;
                       this.lastCalledUpdate = moment();
                       this.firstSetData = true;
                       this.job = success.job;
                       this.jobType = success.typ;
                       if (success.job === true) {
                         const nextStamp = moment.unix(success.nextStamp);
                         if (nextStamp.isAfter()) {
                           this.whoreJobProgressBar();
                         }else {
                           this.whoreJobManual();
                         }
                       }else {
                         this.panelTitle = 'Nudzę się...';
                       }
                     }
                   );
    this.subs.push(sub);
  }

  private whoreJobProgressBar() {
    const diffrence = this.sheduleData.nextStamp - moment().unix();
    this.secondsCounting = diffrence;
    this.allSeconds = this.sheduleData.nextStamp - this.sheduleData.refferStamp;
    this.barPercent = 100;
    // this.calculateProgressBar();
    this.checkRideBac();
    this.jobProgressBar = true;
  }


  private whoreJobCountdown() {
    if (this.jobProgressBar === true) {
      if (this.secondsCounting <= 0) {
        this.jobProgressBar = false;
        this.updateAccount();
      }
      this.secondsCounting--;
      // this.calculateProgressBar();
      this.checkJobLengthAvailable();
      this.checkRideBac();
    }
  }

  private mainIntervalLoop() {
    this.mainInterval = setInterval(() => {
      if (this.job) {
        this.whoreJobCountdown();
        this.whoreJobManual();
      }
    }, 1000);
  }

  private whoreJobManual() {
    const unixStamp = moment().unix();
    if (this.jobProgressBar === false) {
      this.checkJobLengthAvailable();
      if (unixStamp >= this.sheduleData.endStamp) {
        this.updateAccount();
      }
      this.checkRideBac();
    }
  }


  private checkJobLengthAvailable() {
    const unixStamp = moment().unix();
    const jobSeconds = (8 + this.sheduleData.locationPlusMinutes) * 60 * this.sheduleData.fasterComeBackFactor;
    const diffrence = this.sheduleData.endStamp - unixStamp;
    this.jobFast = (diffrence >= (1 * jobSeconds)) ? true : false;
    this.jobMid = (diffrence >= (2 * jobSeconds)) ? true : false;
    this.jobLong = (diffrence >= (3 * jobSeconds)) ? true : false;
  }

  private checkRideBac() {
    const unixStamp = moment().unix();
    if (unixStamp >= this.sheduleData.nextStamp ||  this.sheduleData.nextStamp > this.sheduleData.endStamp) {
      this.rideBack = (!this.jobFast && !this.jobMid && !this.jobLong) ? true : false;
      if (this.rideBack) {
        this.jobProgressBar = true;
        this.secondsCounting = this.sheduleData.endStamp - moment().unix();
      }
    }
  }


/*
  private calculateProgressBar() {
    let secondsLeft = this.allSeconds - this.secondsCounting;
    this.barPercent = parseInt((100 - (secondsLeft / this.allSeconds * 100)).toString(), 10);
  }
*/
  private setPanelTitle(locationType: string) {
    switch (locationType) {
      case 'latarnia':
        this.panelTitle = 'Ruchanko?';
        break;
      case 'odpoczynek':
        this.panelTitle = 'Spanko zzz... ';
        break;
      case 'bar':
        this.panelTitle = 'Ciężko pracuję!';
      break;
    }
  }

  private watchForBrowserTabChanges() {
    window.onblur = () => {
      if (this.lastCalledUpdate) {
        const tempTime = moment(this.lastCalledUpdate.valueOf());
        if (moment().isAfter(tempTime.add(60, 'seconds'))) {
          this.updateAccount();
        }
      }
    };
    window.onfocus = () => {
      if (this.lastCalledUpdate) {
        const tempTime = moment(this.lastCalledUpdate.valueOf());
        if (moment().isAfter(tempTime.add(60, 'seconds'))) {
          this.updateAccount();
        }
      }
    };

  }

  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
    clearInterval(this.mainInterval);
  }
}
