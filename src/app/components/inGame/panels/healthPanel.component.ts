import { Component, OnInit, OnDestroy } from '@angular/core';
import { PanelsService } from './../../../services/inGame/panelsService';
import { HealthInfo } from './../../../responses/inGame/coreResponse';


@Component({
  selector: 'as-healthPanel',
  templateUrl: 'app/components/inGame/panels/healthPanel.html',
  styles: [`
  .table-responsive > .table {
    background-color: transparent;
  }
  .table {
    font-size: small;
  }
  `]
})

export class HealthPanelComponent implements OnInit, OnDestroy {
    private subs = [];
    isMobile: boolean = false;
    isBodyShow: boolean = false;

    healthInfo: HealthInfo;

    constructor(private panelsService: PanelsService) { }

    ngOnInit() {
      this.isMobile = window.innerWidth <= 768 ? true : false;
      this.isBodyShow = this.isMobile ? false : true;
      this.watchHealthData();
    }
    showBody() {
      if (this.isMobile) {
        this.isBodyShow = this.isBodyShow ? false : true;
      }
    }


    private watchHealthData() {
      const sub = this.panelsService.$healthData
                     .subscribe(
                       success => {
                         this.healthInfo = success;
                       }
                     );
      this.subs.push(sub);
    }

    ngOnDestroy() {
      this.subs.map(sub => {
        sub.unsubscribe();
      });
    }
}
