import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RaportsComponent } from './raports.component';

import { RaportsService } from './../../../services/inGame/raportsService';

import { SharedModule } from './../shared.module';

@NgModule({
    declarations: [
      RaportsComponent
    ],
    imports: [
      BrowserModule,
      FormsModule,
      SharedModule
    ],
    exports: [
      RaportsComponent
    ],
    providers: [RaportsService],
})
export class RaportsModule {
}
