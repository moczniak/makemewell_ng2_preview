import { Component, OnInit, OnDestroy } from '@angular/core';

import { RaportsService } from './../../../services/inGame/raportsService';
import { ErrorService } from './../../../services/inGame/errorService';

import { SocketService } from './../../../services/socketService';

import { RaportModel } from './../../../models/raportModel';

@Component({
  templateUrl: 'app/components/inGame/raports/raports.html',
  styleUrls: ['app/components/inGame/raports/raports.css']
})


export class RaportsComponent implements OnInit, OnDestroy {

  private subs = [];

  mainRaports: Array<RaportModel> = [];
  mainCurrentPage: number;
  mainLastPage: number;

  archiveRaports: Array<RaportModel> = [];
  archiveCurrentPage: number;
  archiveLastPage: number;

  constructor(private raportsService: RaportsService, private errorService: ErrorService, private socketService: SocketService) { }

  ngOnInit() {
    this.getMainRaports(1);
    this.getArchiveRaports(1);
    this.watchForNewRaport();
  }

  private watchForNewRaport() {
    const sub = this.socketService.$newRaport
                    .subscribe(
                      value => this.getMainRaports(1)
                    );
    this.subs.push(sub);
  }

  private getMainRaports(page: number) {
    const sub = this.raportsService.getMainRaports(page)
                    .subscribe(
                      success => {
                        this.mainCurrentPage = success.data.current_page;
                        this.mainLastPage = success.data.last_page;
                        this.mainRaports = success.data.data;
                      },
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }
  private getArchiveRaports(page: number) {
    const sub = this.raportsService.getArchiveRaports(page)
                    .subscribe(
                      success => {
                        this.archiveCurrentPage = success.data.current_page;
                        this.archiveLastPage = success.data.last_page;
                        this.archiveRaports = success.data.data;
                      },
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }
  mainSelectPage(page: number) {
    this.getMainRaports(page);
  }
  archiveSelectPage(page: number) {
    this.getArchiveRaports(page);
  }

  openRaport(raportID: number, index: number) {

  }
  deleteMainRaport(raportID: number, index: number) {
    const sub = this.raportsService.deleteRaport(raportID)
                    .subscribe(
                      success => {
                        this.mainRaports.splice(index, 1);
                        if (this.mainRaports.length === 0) {
                          this.getMainRaports(this.mainCurrentPage);
                        }
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }
  deleteArchiveRaport(raportID: number, index: number) {
    const sub = this.raportsService.deleteRaport(raportID)
                    .subscribe(
                      success => {
                        this.mainRaports.splice(index, 1);
                        if (this.mainRaports.length === 0) {
                          this.getMainRaports(this.mainCurrentPage);
                        }
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }

}
