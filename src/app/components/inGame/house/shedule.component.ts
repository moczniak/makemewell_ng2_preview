import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorService } from './../../../services/inGame/errorService';
import { SuccessService } from './../../../services/inGame/successService';
import { HouseService } from './../../../services/inGame/houseService';
import { SkillDescModel } from './../../../models/skillDescModel';
import { VipService } from './../../../services/inGame/vipService';
import { CoreService } from './../../../services/inGame/coreService';
import { PanelsService } from './../../../services/inGame/panelsService';
import { LocationModel } from './../../../models/locationModel';
import * as moment from 'moment';
import { findIndex } from 'lodash';
declare var jQuery: any;

@Component({
  templateUrl: 'app/components/inGame/house/shedule.html',
  styleUrls: ['app/components/inGame/house/shedule.css'],
})


export class SheduleComponent implements OnInit, OnDestroy {

  private subs: Array<any> = [];

  skillsData: Array<SkillDescModel>;

  locationsLocked: Array<LocationModel>;
  locationsUnlocked: Array<LocationModel>;
  locationsToBuy: Array<LocationModel>;

  setActivatedLocations: Array<LocationModel> = [];
  setLocations: Array<LocationModel> = [];
  modalLocation: LocationModel;

  barHour: number;
  jobMinutes: number;
  alfonsMode: boolean;
  rubberMode: boolean;
  assMode: boolean;
  babyHour: number;
  finishTime: any = moment();
  defaultFinishTime: any;

  private othersLocations: Array<string> = ['odpoczynek', 'bar', 'pvp', 'orgia'];

  private sheduleLocationsLimit: number;

  constructor(private errorService: ErrorService, private houseService: HouseService, private vipService: VipService,
              private coreService: CoreService, private panelsService: PanelsService, private successService: SuccessService) { }

  ngOnInit() {
    this.tooltipInit();
    this.updateAccount();
    this.getVipSheduleService();
    this.watchForLocationLoadCounter();
    this.getSkillsData();
  }
  private updateAccount(getSettings = true) {
    const sub = this.coreService.updateAccount()
                    .subscribe(
                      success => {
                        this.coreService.vipStatus = success.data.vipStatus;
                        this.coreService.vipFactors = success.data.vipFactors;
                        this.panelsService.setPanelData(success);
                        if (getSettings) {
                          this.getGameSettings();
                        }
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }
  private watchForLocationLoadCounter() {
    const sub = this.panelsService.$loadLocationCounter
                    .subscribe(
                      value => {
                        this.getLocations();
                        this.getLocationsShedule();
                      }
                    );
    this.subs.push(sub);
  }

  private tooltipInit() {
    jQuery('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        trigger : 'hover'
    });
  }
  private slidersInit() {
    const jobMinutes = jQuery('#jobHour').slider({
      tooltip: 'hide',
      min: 30,
      max: 120,
      value: this.jobMinutes
    });
    jobMinutes.on('change', event => {
      this.jobMinutes = event.value.newValue;
      this.calculateFinishTime();
    });
    const babyHour = jQuery('#babyHour').slider({
      tooltip: 'hide',
      min: 1,
      max: 24,
      value: this.babyHour
    });
    babyHour.on('change', event => {
      this.babyHour = event.value.newValue;
    });
    const barHour = jQuery('#barHour').slider({
      tooltip: 'hide',
      min: 1,
      max: 10,
      value: this.barHour
    });
    barHour.on('change', event => {
      this.barHour = event.value.newValue;
    });
  }
  private switchesInit() {
    jQuery('#alfonsMode').bootstrapSwitch('state', this.alfonsMode, true);
    jQuery('#alfonsMode').on('switchChange.bootstrapSwitch', (event, state) => {
      this.alfonsMode = state;
    });
    jQuery('#rubberMode').bootstrapSwitch('state', this.rubberMode, true);
    jQuery('#rubberMode').on('switchChange.bootstrapSwitch', (event, state) => {
      this.rubberMode = state;
    });
    jQuery('#assMode').bootstrapSwitch('state', this.assMode, true);
    jQuery('#assMode').on('switchChange.bootstrapSwitch', (event, state) => {
      this.assMode = state;
    });
  }
  private calculateFinishTime() {
    const addMinutes = this.setLocations.length * this.jobMinutes;
    this.finishTime = moment(this.defaultFinishTime).add(addMinutes, 'minutes');
  }

  private getSkillsData() {
    const sub = this.houseService.getSkillsData()
                    .subscribe(
                      success => {
                        this.skillsData = success.data;
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  private getLocations() {
    const sub = this.houseService.getLocations()
                    .subscribe(
                      success => {
                        this.locationsLocked = success.data.locationsLocked;
                        this.locationsUnlocked = success.data.locationsUnlocked;
                        this.locationsToBuy = success.data.locationsToBuy;
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  private getLocationsShedule() {
    const sub = this.houseService.getLocationsGrafik()
                    .subscribe(
                      success => {
                        this.finishTime = success.data.activatedEndTime === 0 ? moment() : moment.unix(success.data.activatedEndTime);
                        this.defaultFinishTime = Object.assign({}, this.finishTime);
                        this.setActivatedLocations = success.data.locationsActivated;
                        this.setLocations = success.data.locationsNoActivated;
                        this.calculateFinishTime();
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  private getGameSettings() {
    const sub = this.houseService.getGameSettings()
                    .subscribe(
                      success => {
                        this.barHour = success.data.bar_hour;
                        this.jobMinutes = success.data.latarnia_minute;
                        this.alfonsMode = success.data.alfons_mode;
                        this.rubberMode = success.data.bez_gumy;
                        this.assMode = success.data.anal;
                        this.babyHour = success.data.antyplod_hour;
                        this.slidersInit();
                        this.switchesInit();
                        this.getLocations();
                        this.getLocationsShedule();
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }
  private getVipSheduleService() {
    const sub = this.vipService.getVipServices()
                    .subscribe(
                      success => {
                        this.sheduleLocationsLimit = success.data.big_grafik ? 6 : 2;
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }
  tooltipHide($event) {
    const element = <HTMLElement> $event.target;
    const tooltipID = element.getAttribute('aria-describedby');
    jQuery('#' + tooltipID).remove();
  }
  selectLocation(index) {
    if (this.setLocations.length + this.setActivatedLocations.length <= this.sheduleLocationsLimit - 1) {
      this.setLocations.push(Object.assign({}, this.locationsUnlocked[index]));
      this.locationsUnlocked.splice(index, 1);
      this.calculateFinishTime();
    }else {
      this.errorService.setError('Osiągnełaś limit kafelków, by mieć więcej musisz aktywować premium kafelki');
    }
  }
  diselectLocation(index) {
    this.locationsUnlocked.push(Object.assign({}, this.setLocations[index]));
    this.setLocations.splice(index, 1);
    this.calculateFinishTime();
  }
  prepareUnlockedDescription(location: LocationModel) {
    let jobMinutes = this.jobMinutes;
    if (this.othersLocations.indexOf(location.code) !== -1) {
      jobMinutes = this.getMinutesForOtherLocations(location);
    }
    return `<b><center>${location.nazwa}</center></b><br>
            ${location.opis}<br>
            <b>czas trwania:</b>${jobMinutes}`;
  }
  prepareLockedDescription(location: LocationModel) {
    return `<b><center>${location.nazwa}</center></b><br>
            ${location.opis}<br>
            <b>Kursy:</b><br>
              ${this.prepareCoursesRequirement(location)}
            <b>Wymagania:</b><br>
            Prestiż:${location.prestizn}`;
  }

  prepareCoursesRequirement(location: LocationModel) {
    let text = '';
    this.skillsData.map(skill => {
      text += `<li><i>${skill.nazwa}: ${location['sid' + skill.sid]}</i></li>`;
    });
    return text;
  }

  private getMinutesForOtherLocations(location: LocationModel): number {
    let jobMinutes = 0;
    switch (location.code) {
      case 'pvp':
        jobMinutes = 30;
        break;
      case 'bar':
        jobMinutes = this.barHour * 60;
      break;
      case 'odpoczynek':
        let tmpFactor = this.coreService.vipStatus.fasterChcicaReg ? this.coreService.vipFactors.fasterChcicaReg : 1;
        jobMinutes = 120 * tmpFactor;
      break;
    }
    return jobMinutes;
  }

  saveGameSettings() {
    const preparedObject = {
      jobMinutes: this.jobMinutes,
      alfonsMode: this.alfonsMode,
      rubberMode: this.rubberMode,
      assMode: this.assMode,
      babyHour: this.babyHour,
      barHour: this.barHour
    };
    const sub = this.houseService.sendGameSettings(preparedObject)
                    .subscribe(
                        success => {
                          this.successService.setSuccess('Ustawienia zapisane!');
                          this.updateAccount(false);
                        }
                    );
    this.subs.push(sub);
  }
  sheduleUpdate() {
    const locationsIDArray = this.setLocations.map(location => location.code);
    const sub = this.houseService.sendSheduleUpdate(locationsIDArray)
                    .subscribe(
                      success => {
                        this.finishTime = moment.unix(success.data.activeEndTime);
                        const locationIndex = findIndex(this.setLocations, {code: success.data.activeLocation});
                        if (locationIndex !== -1) {
                          this.setActivatedLocations.push(Object.assign({}, this.setLocations[locationIndex]));
                          this.setLocations.splice(locationIndex, 1);
                        }
                        this.updateAccount();
                        this.successService.setSuccess('Grafik zapisany! jedziem z kurwami! tfu... klientami hihi');
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }

}
