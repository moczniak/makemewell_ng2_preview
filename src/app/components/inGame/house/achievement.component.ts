import { Component, OnInit } from '@angular/core';
import { HouseService } from './../../../services/inGame/houseService';
import { AchievementModel } from './../../../models/achievementModel';


@Component({
  templateUrl: 'app/components/inGame/house/achievement.html',
  styleUrls: ['app/components/inGame/house/achievement.css']
})


export class AchievementComponent implements OnInit {

  completed: Array<AchievementModel>;
  available: Array<AchievementModel>;


  constructor(private houseService: HouseService) { }

  ngOnInit() {
    this.houseService.getAchievements()
        .subscribe(
            success => {
              console.log(success);
              this.completed = success.data.completed;
              this.available = success.data.available;
            },
            error => console.log(error)
        );
  }


}
