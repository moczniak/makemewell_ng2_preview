import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AchievementComponent } from './achievement.component';
import { HouseService } from './../../../services/inGame/houseService';
import { SharedModule } from './../shared.module';

@NgModule({
    declarations: [
        AchievementComponent,
    ],
    imports: [
      BrowserModule,
      FormsModule,
      SharedModule
    ],
    exports: [
      AchievementComponent
    ],
    providers: [HouseService],
})
export class AchievementModule {
}
