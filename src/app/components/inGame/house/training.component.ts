import { Component, OnInit, OnDestroy } from '@angular/core';
import { HouseService } from './../../../services/inGame/houseService';
import { VipService } from './../../../services/inGame/vipService';
import { ErrorService } from './../../../services/inGame/errorService';
import { SuccessService } from './../../../services/inGame/successService';
import { PanelsService } from './../../../services/inGame/panelsService';
import { SkillUserData } from './../../../responses/inGame/houseResponse';
import { NumberFormatPipe } from './../../../pipes/numberFormat.pipe';
import { SecondsToStrPipe } from './../../../pipes/secondsToStr.pipe';
import { CoreService } from './../../../services/inGame/coreService';
declare var jQuery: any;

@Component({
  templateUrl: 'app/components/inGame/house/training.html',
  styleUrls: ['app/components/inGame/house/training.css']
})


export class TrainingComponent implements OnInit, OnDestroy {

  private subs = [];

  activeTraining: SkillUserData;
  activeTrainingInterval: any;
  waitingTraining: Array<SkillUserData> = [];
  availableTrainings: Array<SkillUserData>;
  trainingPoints: number;
  private numberFormatPipe: NumberFormatPipe;
  private secondsToStrPipe: SecondsToStrPipe;

  private trainingLimit: number;

  constructor(private houseService: HouseService, private vipService: VipService, private errorService: ErrorService,
              private successService: SuccessService, private panelsService: PanelsService, private coreService: CoreService) {
    this.numberFormatPipe = new NumberFormatPipe();
    this.secondsToStrPipe = new SecondsToStrPipe();
   }

  ngOnInit() {
    this.tooltipInit();
    this.sliderInit();
    this.getSkillsUserData();
    this.getVipSheduleService();
  }

  private getSkillsUserData() {
    const sub = this.houseService.getSkillsUserData()
                    .subscribe(
                      success => {
                        this.availableTrainings = success.data.availableTrainings;
                        this.trainingPoints = success.data.trainingPoints;
                        this.waitingTraining = success.data.waitingTraining;
                        this.activeTraining = success.data.activeTraining;
                        if (this.activeTraining) {
                          this.activeTrainingCountdown();
                        }
                        this.sliderInit();
                      },
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }

  private getVipSheduleService() {
    const sub = this.vipService.getVipServices()
                    .subscribe(
                      success => {
                        this.trainingLimit = success.data.big_kurs ? 7 : 3;
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }

  updateTrainingPoints() {
    const sub = this.houseService.sendTrainingPoints(this.trainingPoints)
                    .subscribe(
                      success => {
                        this.getAvailableSkills();
                        this.successService.setSuccess('Zmiany zostały zapisane!');
                      },
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }

  private getAvailableSkills() {
    const sub = this.houseService.getAvailableSkills()
                    .subscribe(
                      success => {
                        this.availableTrainings = success.data.availableTrainings;
                      },
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }

  trainingUpdate() {
    const trainingIDS = this.waitingTraining.filter(training => !training.trainingID).map(training => training.sid);
    if (trainingIDS.length > 0 ) {
      const sub = this.houseService.sendTraining(trainingIDS)
                      .subscribe(
                        success => {
                          this.panelsService.setPanelData(success);
                          this.waitingTraining = success.data.waitingTraining;
                          this.activeTraining = success.data.activeTraining;
                          if (this.activeTraining) {
                            this.activeTrainingCountdown();
                          }
                        },
                        error => this.errorService.setError(error)
                      );
     this.subs.push(sub);
    }
  }

  private deleteTraining(trainingID) {
    const sub = this.houseService.deleteTraining(trainingID)
                    .subscribe(
                      success => this.panelsService.setPanelData(success),
                      error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }

  private updateAccount() {
    const sub = this.coreService.updateAccount()
                    .subscribe(
                        success => this.getSkillsUserData(),
                        error => this.errorService.setError(error)
                    );
   this.subs.push(sub);
  }


  selectSkill(training: SkillUserData) {
    if (this.waitingTraining.length + this.waitingTraining.length <= this.trainingLimit - 1) {
      this.waitingTraining.push(training);
    }else {
      this.errorService.setError('Osiągnełaś limit szkolenia, by mieć więcej musisz aktywować szkolenie premium');
    }
  }
  diselectSkill(index: number, trainingID: number) {
    if (trainingID) {
      this.deleteTraining(trainingID);
    }
    this.waitingTraining.splice(index, 1);
  }

  prepareAvailableSkillDesc(training: SkillUserData) {
    return `<b><center>${training.nazwa}</center></b><br>
            ${training.opis}<br>
            <b>Poziom:</b>${training.level}<br>
            <b>Ulepszenie:</b> ${training.trainingPoints} pkt<br><hr>
            <b>Koszt:</b> ${this.numberFormatPipe.transform(training.price)}<br>
            <b>Czas trwania:</b> ${this.secondsToStrPipe.transform(training.seconds)}<br>
            `;
  }

  prepareWaitingDesc(training: SkillUserData) {
    return `<b><center>${training.nazwa}</center></b><br>
            ${training.opis}<br>
            <b>Plus:</b> ${training.level} pkt<br>
            <b>Koszt:</b> ${this.numberFormatPipe.transform(training.price)}<br>
            <b>Czas trwania:</b> ${this.secondsToStrPipe.transform(training.seconds)}<br>
            `;
  }


  private activeTrainingCountdown() {
    clearInterval(this.activeTrainingInterval);
    this.activeTrainingInterval = setInterval(() => {
      if (this.activeTraining.seconds <= 0) {
        clearInterval(this.activeTrainingInterval);
        this.updateAccount();
      }
      this.activeTraining.seconds--;
    }, 1000);
  }



  private sliderInit() {
    const trainingPoints = jQuery('#trainingPoints').slider({
      tooltip: 'hide',
      min: 1,
      max: 20,
      value: this.trainingPoints
    });
    trainingPoints.on('change', event => {
      this.trainingPoints = event.value.newValue;
    });
  }

  private tooltipInit() {
    jQuery('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        trigger : 'hover'
    });
  }

  tooltipHide($event) {
    const element = <HTMLElement> $event.target;
    const tooltipID = element.getAttribute('aria-describedby');
    jQuery('#' + tooltipID).remove();
  }

  ngOnDestroy() {
    clearInterval(this.activeTrainingInterval);
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }

}
