import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { TrainingComponent } from './training.component';
import { HouseService } from './../../../services/inGame/houseService';
import { VipService } from './../../../services/inGame/vipService';
import { SharedModule } from './../shared.module';

@NgModule({
    declarations: [
        TrainingComponent,
    ],
    imports: [
      BrowserModule,
      FormsModule,
      SharedModule
    ],
    exports: [
      TrainingComponent
    ],
    providers: [HouseService, VipService],
})
export class TrainingModule {
}
