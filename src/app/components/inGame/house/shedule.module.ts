import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { SheduleComponent } from './shedule.component';
import { SharedModule } from './../shared.module';

import { HouseService } from './../../../services/inGame/houseService';
import { VipService } from './../../../services/inGame/vipService';

@NgModule({
    declarations: [
        SheduleComponent,
    ],
    imports: [
      BrowserModule,
      FormsModule,
      SharedModule
    ],
    exports: [
      SheduleComponent
    ],
    providers: [HouseService, VipService],
})
export class SheduleModule {
}
