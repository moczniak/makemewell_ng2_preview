import { Injectable } from '@angular/core';
import { CONSTANTS } from './../shared/';
import { Subject } from 'rxjs/Subject';

import { NewEventSocket } from './../responses/socketResponse';
import { NewChatMessage } from './../responses/socketResponse';
import { AuthService } from './authService';

declare var io: any;

@Injectable()
export class SocketService {
  $socket: any;

  private newRaport = new Subject<NewEventSocket>();
  $newRaport = this.newRaport.asObservable();

  private newChatMessage = new Subject<NewChatMessage>();
  $newChatMessage = this.newChatMessage.asObservable();

  constructor(private authService: AuthService) { }

   socketConnect() {
     this.$socket = io.connect(CONSTANTS.$HOSTSOCKET);
     this.watchChat();
   }

   joinEventRoom() {
     this.$socket.emit('joinEventRoom', {userToken: this.authService.getAuthToken()});
     this.$socket.on('newEvent', (data: NewEventSocket) => {
       switch (data.type) {
         case 'raports':
          this.newRaport.next(data);
         break;
       }
     });
   }

   joinChatRoom(roomID: number) {
     this.$socket.emit('joinChatRoom', {userToken: this.authService.getAuthToken(), roomID: roomID});
   }

   private watchChat() {
     this.$socket.on('newChatMessage', (data: NewChatMessage) => {
       this.newChatMessage.next(data);
     });
   }

   leaveEventRoom() {
     this.$socket.emit('leaveEventRoom', {userToken: this.authService.getAuthToken()});
   }


}
