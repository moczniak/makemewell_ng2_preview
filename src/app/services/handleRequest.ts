import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

export abstract class HandleRequest {


  protected extractData(res: Response) {
    this['spinnerService'].stopSpinning();
    const data = res.json();
    return data || { };
  }

  protected handleError (error: any) {
    this['spinnerService'].stopSpinning();
    if (error.status === 400) {
      const data = error.json();
      return Observable.throw(data.message || 'Server Error');
    }else if (error.status === 422) {
      const data = error.json();
      return Observable.throw(data || 'Server Error');
    } else if (error.status === 401) {
      this['router'].navigate(['/']);
    }
  }

}
