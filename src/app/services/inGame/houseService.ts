import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';

import { GameSettings, Locations, LocationsShedule, SheduleUpdate,
         TrainingSkills, SheduleSkillsData, TrainingDelete, TrainingUpdate,
         AvailableSkills, Achievements } from './../../responses/inGame/houseResponse';

@Injectable()
export class HouseService extends HandleRequest {

   private headers = new Headers({ 'Content-Type': 'application/json' });
   private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router) {
    super();
  }

  getLocations(): Observable<Locations> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/locations' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getLocationsGrafik(): Observable<LocationsShedule> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/locationsShedule' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getGameSettings(): Observable<GameSettings> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/gameSettings' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendSheduleUpdate(shedule: Array<string>): Observable<SheduleUpdate> {
     this.spinnerService.showSpinning();
     let body = JSON.stringify({shedule});
     return this.http.put(CONSTANTS.$HOSTGAME + '/shedule' + '?token=' + this.authService.getAuthToken(), body, this.options)
                     .map(this.extractData.bind(this))
                     .catch(this.handleError.bind(this));
  }

  sendGameSettings(settings: Object) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify(settings);
    return this.http.put(CONSTANTS.$HOSTGAME + '/gameSettings' + '?token=' + this.authService.getAuthToken(), body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getSkillsData(): Observable<SheduleSkillsData> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/skillsData' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getSkillsUserData(): Observable<TrainingSkills> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/skillsUserData' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getAvailableSkills(): Observable<AvailableSkills> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/availableSkills' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendTrainingPoints(trainingPoints: number) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({trainingPoints});
    return this.http.put(CONSTANTS.$HOSTGAME + '/trainingPoints' + '?token=' + this.authService.getAuthToken(), body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendTraining(trainings: Array<number>): Observable<TrainingUpdate> {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({trainings});
    return this.http.put(CONSTANTS.$HOSTGAME + '/training' + '?token=' + this.authService.getAuthToken(), body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  deleteTraining(trainingID: number): Observable<TrainingDelete> {
    this.spinnerService.showSpinning();
    return this.http.delete(CONSTANTS.$HOSTGAME + '/training/' + trainingID + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getAchievements(): Observable<Achievements> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/achievement' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

}
