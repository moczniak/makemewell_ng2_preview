import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';
import { Subject } from 'rxjs/Subject';
import { UpdateAccount, Shedule, UserInfo, HealthInfo } from './../../responses/inGame/coreResponse';

@Injectable()
export class PanelsService extends HandleRequest {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });


  private sheduleData = new Subject<Shedule>();
  $sheduleData = this.sheduleData.asObservable();

  private healthData = new Subject<HealthInfo>();
  $healthData = this.healthData.asObservable();

  private userData = new Subject<UserInfo>();
  $userData = this.userData.asObservable();

  private loadLocationCounter = new Subject<number>();
  $loadLocationCounter = this.loadLocationCounter.asObservable();

 constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router) {
   super();
 }

  setPanelData(data: UpdateAccount) {
    if (data.data.shedule) {
      this.sheduleData.next(data.data.shedule);
    }
    if (data.data.healthInfo) {
      this.healthData.next(data.data.healthInfo);
    }
    if (data.data.userInfo) {
      this.userData.next(data.data.userInfo);
    }
  }


  setLoadLocationCounter(count: number) {
    this.loadLocationCounter.next(count);
  }

  setActiveSheduleUpdateOption(option: number) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({option});
    return this.http.put(CONSTANTS.$HOSTGAME +
                    '/activeSheduleUpdateOption' + '?token=' + this.authService.getAuthToken(), body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }


}
