import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';

import { VipStatus } from './../../responses/inGame/vipResponse';



@Injectable()
export class VipService extends HandleRequest {

   private headers = new Headers({ 'Content-Type': 'application/json' });
   private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router) {
    super();
  }

  getVipServices(): Observable<VipStatus> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/vipServices' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendLoginForm(form: any) {
     this.spinnerService.showSpinning();
     let body = JSON.stringify(form);
     return this.http.post(CONSTANTS.$HOST + '/login', body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

}
