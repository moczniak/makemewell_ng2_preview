import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';

import { MainRaports } from './../../responses/inGame/raportsResponse';


@Injectable()
export class RaportsService extends HandleRequest {

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router) {
    super();
  }

  deleteRaport(raportID: number) {
    this.spinnerService.showSpinning();
    return this.http.delete(CONSTANTS.$HOSTGAME + '/raports/' + raportID + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }
  getMainRaports(page: number): Observable<MainRaports> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/mainRaports' + '?page=' + page + '&token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }
  getArchiveRaports(page: number): Observable<MainRaports> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/archiveRaports' + '?page=' + page + '&token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }


}
