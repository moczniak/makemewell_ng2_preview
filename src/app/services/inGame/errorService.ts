import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ErrorService {
  private errorMessage = new Subject<string>();
  $errorMessage = this.errorMessage.asObservable();


  setError(message: string) {
    this.errorMessage.next(message);
  }

}
