import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';
import { BarUserInfo, BarSlotsInfo, BarSlotsPlay } from './../../responses/inGame/barResponse';

@Injectable()
export class BarService extends HandleRequest {

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router) {
    super();
  }

  getUserInfo(): Observable<BarUserInfo> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/barUserInfo' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  getSlotsInfo(): Observable<BarSlotsInfo> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/slotsInfo' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  playSlots(multiplier: number): Observable<BarSlotsPlay> {
    return this.http.get(CONSTANTS.$HOSTGAME + '/slotsPlay/' + multiplier + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }


}
