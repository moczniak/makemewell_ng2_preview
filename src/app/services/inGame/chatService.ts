import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../../shared/';
import { HandleRequest } from './../handleRequest';
import { SpinnerService } from './../spinnerService';
import { AuthService } from './../authService';
import { SocketService } from './../socketService';
import { RoomList } from './../../responses/inGame/chatResponse';

@Injectable()
export class ChatService extends HandleRequest {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService, private router: Router,
              private socketService: SocketService) {
    super();
  }

  getChatRoomList(): Observable<RoomList> {
    this.spinnerService.showSpinning();
    return this.http.get(CONSTANTS.$HOSTGAME + '/chatRoomList' + '?token=' + this.authService.getAuthToken())
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendMessage(message: string, roomID: number) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({message});
    return this.http.post(CONSTANTS.$HOSTGAME + '/chatRoomMessage/' + roomID + '?token=' +
                                              this.authService.getAuthToken(), body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }


}
