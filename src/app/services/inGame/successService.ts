import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SuccessService {
  private successMessage = new Subject<string>();
  $successMessage = this.successMessage.asObservable();


  setSuccess(message: string) {
    this.successMessage.next(message);
  }

}
