import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

declare var ENV: string;


@Injectable()
export class AuthService implements CanActivate {

  private authToken: string = '';
  private isAuth: boolean = false;
  tokenQuery: string;

  constructor(private router: Router) {
    let token: string = localStorage.getItem('mmwAuthToken');
    if (typeof token !== 'undefined' && token !== null) {
      this.authToken = token;
      this.setTokenQuery();
    }
  }

  private setTokenQuery() {
    this.tokenQuery = '?token=' + this.authToken;
  }

  setAuthToken(token: string) {
    this.authToken = token;
    localStorage.setItem('mmwAuthToken', token);
    this.setTokenQuery();
  }
  getAuthToken() {
    return this.authToken;
  }

  removeAuthToken() {
    this.authToken = '';
    localStorage.removeItem('mmwAuthToken');
    this.isAuth = false;
    this.setTokenQuery();
  }

  setAuthStatus(value: boolean) {
    this.isAuth = value;
  }

  canActivate() {
    if (ENV === 'production') {
      if (this.isAuth) {
        return true;
      }
      this.router.navigate(['/']);
      return false;
    }else {
      return true;
    }
  }



}
