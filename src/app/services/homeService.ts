import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CONSTANTS } from './../shared/';
import { LoginForm } from './../forms/loginForm';
import { RegisterForm } from './../forms/registerForm';
import { ForgetNewPasswordForm } from './../forms/forgetNewPasswordForm';
import { HandleRequest } from './handleRequest';
import { SpinnerService } from './spinnerService';
import { AuthService } from './authService';

import { LoginData, Login } from './../responses/homeResponse';

@Injectable()
export class HomeService extends HandleRequest {


  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, private spinnerService: SpinnerService, private authService: AuthService) {
    super();
  }

  sendLoginForm(form: LoginForm): Observable<Login> {
     this.spinnerService.showSpinning();
     let body = JSON.stringify(form);
     return this.http.post(CONSTANTS.$HOST + '/login', body, this.options)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  checkToken(token: string): Observable<LoginData> {
    return this.http.get(CONSTANTS.$HOST + '/checkToken' + '?token=' + token)
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  sendRegisterForm(form: RegisterForm) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify(form);
    return this.http.post(CONSTANTS.$HOST + '/register', body, this.options)
                   .map(this.extractData.bind(this))
                   .catch(this.handleError.bind(this));
  }

  sendForgetPasswordEmail(email: string) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({email});
    return this.http.post(CONSTANTS.$HOST + '/forgetPasswordEmail', body, this.options)
                   .map(this.extractData.bind(this))
                   .catch(this.handleError.bind(this));
  }

  sendForgetNewPasswordForm(form: ForgetNewPasswordForm) {
    this.spinnerService.showSpinning();
    let body = JSON.stringify(form);
    return this.http.post(CONSTANTS.$HOST + '/forgetNewPassword', body, this.options)
                   .map(this.extractData.bind(this))
                   .catch(this.handleError.bind(this));
  }

  createWorld(world: number): Observable<Login> {
    this.spinnerService.showSpinning();
    let body = JSON.stringify({world});
    return this.http.post(CONSTANTS.$HOST + '/createWorld' + this.authService.tokenQuery, body, this.options)
                   .map(this.extractData.bind(this))
                   .catch(this.handleError.bind(this));
  }

  getHeroes(): Observable<any[]> {
    return this.http.get(CONSTANTS.$HOST)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

}
