import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SpinnerService {

  private spinner = new Subject<boolean>();
  isSpinning$ = this.spinner.asObservable();

  showSpinning() {
    this.spinner.next(true);
  }

  stopSpinning() {
      this.spinner.next(false);
  }


}
