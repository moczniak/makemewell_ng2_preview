import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment/min/locales';

@Pipe({name: 'timeStr'})
export class TimeStrPipe implements PipeTransform {


  transform(value: any, exponent = false): string {
    if (typeof value === 'number') {
      return moment.unix(value).locale('pl').calendar(null, {
        sameElse: 'DD/MM/YYYY HH:mm'
      });
    }else {
      return moment(value).locale('pl').calendar();
    }
  }
}
