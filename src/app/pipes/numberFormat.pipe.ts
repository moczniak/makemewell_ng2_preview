import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'numberFormat'})
export class NumberFormatPipe implements PipeTransform {

  private numberToMoney(value, decPlaces = 2, thouSeparator = '.', decSeparator = ',') {
    let sign;
    let i;
    let j;

    sign = value < 0 ? '-' : '';
    i = parseInt(value = Math.abs(+value || 0).toFixed(decPlaces), 10) + '';
    j = (j = i.length) > 3 ? j % 3 : 0;

    return sign + (j ? i.substr(0, j) + thouSeparator : '')
     + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1'
     + thouSeparator) + (decPlaces ? decSeparator + Math.abs(value - i).toFixed(decPlaces).slice(2) : '');
  }

  transform(value: number): string {
    return this.numberToMoney(value);
  }
}
