import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment/min/locales';

@Pipe({name: 'secondsStr'})
export class SecondsToStrPipe implements PipeTransform {


  transform(value: number): string {
    const duration = moment.duration(value, 'seconds');
    return ('0'  + duration.minutes()).slice(-2) + ':' + ('0'  + duration.seconds()).slice(-2);
  }
}
