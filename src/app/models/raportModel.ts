export interface RaportModel {
  rid?: number;
  uid?: number;
  typ?: string;
  timeget?: number;
  tytul?: string;
  wynik?: string;
  status?: boolean;
  read?: boolean;
  folder?: string;
}
