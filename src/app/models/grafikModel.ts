export interface GrafikModel {
  gid?: number;
  uid?: number;
  typ?: string;
  location?: string;
  locTimeLimit?: number;
  start_time?: number;
  end_time?: number;
  stamp?: number;
  option?: number;
  start?: number;
}
