export interface AchievementModel {
  osid?: number;
  nazwa?: string;
  level?: number;
  typ?: string;
  wym?: number;
  txt?: string;
  vip?: number;
}
