export interface VipServicesStatus {
  pvpInstant: boolean;
  pvpLock: boolean;
  burdelPremiumView: boolean;
  fasterComeBack: boolean;
  fasterChcicaReg: boolean;
  moreHajsKlient: boolean;
  Burdel5Kolejka: boolean;
  big_grafik: boolean;
  big_kurs: boolean;
  chcicaReg: boolean;
}
