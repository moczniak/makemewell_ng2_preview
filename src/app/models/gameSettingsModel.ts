export interface GameSettingsModel {
  settingID?: number;
  uid?: number;
  bar_hour?: number;
  bar_paid?: number;
  anal?: boolean;
  bez_gumy?: boolean;
  latarnia_minute?: number;
  antyplod_hour?: number;
  antyplod_taked?: number;
  antyplod_timeset?: number;
  alfons_mode?: boolean;
  kurs_amount?: number;
  styl?: string;
  jezyk?: string;
  backgroundLink?: string;
  opis?: string;
}
