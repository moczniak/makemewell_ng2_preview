export interface SkillDescModel {
  sid?: number;
  nazwa?: string;
  opis?: string;
  img_path?: string;
}
