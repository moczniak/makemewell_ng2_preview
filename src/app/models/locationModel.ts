export interface LocationModel {
  mid?: number;
  code?: string;
  unlock?: boolean;
  nazwa?: string;
  k_hajs?: number;
  k_prestiz?: number;
  prestizn?: number;
  wsp?: number;
  sid1?: number;
  sid2?: number;
  sid3?: number;
  sid4?: number;
  sid5?: number;
  opis?: string;
  image_icon?: string;
  specialLong?: boolean;
}
