import { NgModule } from '@angular/core';
import { appRoutingProviders, routing } from './app.routing';
import { HttpModule } from '@angular/http';


import { APP_PROVIDERS } from './app.providers';
import { AppComponent } from './app.component';

import { HomeModule } from './components/home/home.module';
import { GameModule } from './components/inGame/game.module';
import { SpinnerComponent } from './helpers/spinner.component';
import { SpinnerService } from './services/spinnerService';
import { AuthService } from './services/authService';
import { SocketService } from './services/socketService';

@NgModule({
    declarations: [
        AppComponent,
        SpinnerComponent,
    ],
    imports: [
        HomeModule,
        GameModule,
        routing,
        HttpModule,

    ],
    providers: [ APP_PROVIDERS, appRoutingProviders, SpinnerService, AuthService, SocketService ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
}
