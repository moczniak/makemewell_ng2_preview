import { RoomMessage } from './inGame/chatResponse';

export interface NewEventSocket {
  type?: string;
  dataID?: number;
}

export interface NewChatMessage {
  data?: RoomMessage;
}
