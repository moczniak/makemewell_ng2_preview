export interface LoginData {
  data?: {
    t1_uid?: number
  };
}
export interface Login {
  data?: {
    t1_uid?: number,
    authToken?: string
  };
}
