import { RaportModel } from './../../models/raportModel';

export interface MainRaports {
  data?: {
    current_page?: number;
    from?: number;
    last_page?: number;
    next_page_url?: string;
    per_page?: number;
    prev_page_url?: string;
    to?: number;
    total?: number;
    data?: Array<RaportModel>
  };
}
