export interface HealthInfo {
  desire?: number;
  period?: boolean;
  pregnancy?: boolean;
  disease?: boolean;
}

export interface UserInfo {
  money?: number;
  prestige?: number;
  vip?: number;
  fame?: number;
}

export interface VipStatus {
  pvpInstant: boolean;
  pvpLock: boolean;
  burdelPremiumView: boolean;
  fasterComeBack: boolean;
  fasterChcicaReg: boolean;
  moreHajsKlient: boolean;
  Burdel5Kolejka: boolean;
  big_grafik: boolean;
  big_kurs: boolean;
  chcicaReg: boolean;
}

export interface VipFactors {
  fasterComeBack: number;
  fasterChcicaReg: number;
  moreHajsKlient: number;
}

export interface Shedule {
    job?: boolean;
    typ?: string;
    nextStamp?: number;
    refferStamp?: number;
    fasterComeBackFactor?: number;
    endStamp?: number;
    locationPlusMinutes?: number;
}

export interface UpdateAccount {
  data?: {
    shedule?: Shedule;
    healthInfo?: HealthInfo;
    userInfo?: UserInfo;
    vipStatus?: VipStatus;
    vipFactors?: VipFactors;
  };
}
