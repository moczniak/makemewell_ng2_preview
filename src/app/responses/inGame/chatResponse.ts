export interface RoomMessage {
  post_id?: number;
  uid?: number;
  login?: string;
  room?: number;
  tekst?: string;
  post_date?: string;
}

export interface RoomDetail {
  roomID?: number;
  name?: string;
  messages?: Array<RoomMessage>;
}

export interface RoomList {
  data?: Array<RoomDetail>;
}
