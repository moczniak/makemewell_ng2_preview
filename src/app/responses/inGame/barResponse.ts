import { UserInfo } from './coreResponse';

export interface UserInfo {
  positionName?: string;
  hourMoney?: number;
  timeToNextPosition?: number;
}
export interface BarUserInfo {
  data?: UserInfo;
}

export interface SlotsPointsPrice {
  cost?: number;
  amount?: number;
}
export interface SlotsReward {
  name?: string;
  type?: string;
  multiplier?: number;
  rewardType?: string;
}
export interface SlotsInfo {
  slotsPointsPrices?: Array<SlotsPointsPrice>;
  slotsPoints?: number;
  slotsRewards?: Array<SlotsReward>;
  columns?: Array<Array<string>>;
}
export interface BarSlotsInfo {
  data?: SlotsInfo;
}


export interface SlotsWinnerReward {
  type?: string;
  reward?: string;
}
export interface SlotsPlay {
  columns?: Array<Array<string>>;
  reward?: SlotsWinnerReward;
  userInfo?: UserInfo;
  slotsPoints?: number;
}
export interface BarSlotsPlay {
  data?: SlotsPlay;
}
