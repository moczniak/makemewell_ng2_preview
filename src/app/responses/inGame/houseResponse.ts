import { GameSettingsModel } from './../../models/gameSettingsModel';
import { LocationModel } from './../../models/locationModel';
import { SkillDescModel } from './../../models/skillDescModel';
import { AchievementModel } from './../../models/achievementModel';
import { UserInfo } from './coreResponse';

export interface GameSettings {
  data?: GameSettingsModel;
}

export interface Locations {
  data?: {
    locationsLocked?: Array<LocationModel>;
    locationsUnlocked?: Array<LocationModel>;
    locationsToBuy?: Array<LocationModel>;
  };
}

export interface LocationsShedule {
  data?: {
    locationsActivated?: Array<LocationModel>;
    locationsNoActivated?: Array<LocationModel>;
    activatedEndTime?: number;
  };
}

export interface SheduleUpdate {
  data?: {
    activeEndTime?: number;
    activeLocation?: string;
  };
}


export interface SkillUserData extends SkillDescModel {
  level?: number;
  price?: number;
  seconds?: number;
  trainingPoints?: number;
  trainingID?: number;
}

export interface TrainingSkills {
  data?: {
    availableTrainings?: Array<SkillUserData>;
    waitingTraining?: Array<SkillUserData>;
    activeTraining?: SkillUserData;
    trainingPoints?: number;
  };
}

export interface SheduleSkillsData {
  data?: Array<SkillDescModel>;
}

export interface AvailableSkills {
  data?: {
    availableTrainings?: Array<SkillUserData>;
  };
}

export interface TrainingUpdate {
  data?: {
    userInfo?: UserInfo;
    waitingTraining?: Array<SkillUserData>;
    activeTraining?: SkillUserData;
  };
}

export interface TrainingDelete {
  data?: {
    userInfo?: UserInfo;
  };
}


export interface Achievements {
  data?: {
    completed?: Array<AchievementModel>;
    available?: Array<AchievementModel>;
  };
}
