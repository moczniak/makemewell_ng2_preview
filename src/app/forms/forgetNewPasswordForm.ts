export class ForgetNewPasswordForm {
  public password: string;
  public passwordRepeat: string;
  public token: string;

  constructor(
    password: string,
    passwordRepeat: string,
    token: string
  ) { }
}
