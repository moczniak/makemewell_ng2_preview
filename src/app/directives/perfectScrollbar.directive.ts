import { Directive, ElementRef, Renderer, Input, OnDestroy } from '@angular/core';
declare var Ps: any;

@Directive({ selector: '[perfectScrollbar]' })
export class PerfectScrollbarDirective implements OnDestroy {

  private element;
  private stickBottomStatus: boolean = false;
  private observer: any;
  private firstSet: boolean = false;

    constructor(el: ElementRef, renderer: Renderer) {
       this.element = el.nativeElement;
       this.setScroll();
    }

    @Input() set stickBottom(status: boolean) {
      this.stickBottomStatus = status || this.stickBottomStatus;
      this.watchForContentChange();
    }

    private setScroll() {
      Ps.initialize(this.element);
    }

    private watchForContentChange() {
      this.observer = new MutationObserver(mutations => {
        if (this.firstSet === false) {
          setTimeout(() => {
            this.element.scrollTop = this.element.scrollHeight;
            Ps.update(this.element);
            this.firstSet = true;
          }, 1000);
        }else {
          this.element.scrollTop = this.element.scrollHeight;
          Ps.update(this.element);
        }
      });
      this.observer.observe(this.element, {childList: true});
    }

    ngOnDestroy() {
      this.observer.disconnect();
    }
}
