import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorPopupComponent } from './errorPopup.component';
import { SuccessPopupComponent } from './successPopup.component';


@NgModule({
    declarations: [
      ErrorPopupComponent,
      SuccessPopupComponent,
    ],
    imports: [
      BrowserModule
    ],
    exports: [
      ErrorPopupComponent,
      SuccessPopupComponent,
    ]
})
export class HelpersModule {
}
