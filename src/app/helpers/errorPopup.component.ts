import {Component , Input , OnChanges , SimpleChange} from '@angular/core';


@Component({
  selector: 'as-errorPopup',
  template: `<div [hidden]="!show2" class="alert alert-danger">
                {{messageParsed}}
                <ul>
                  <li *ngFor="let error of messageArray">{{error}}</li>
                </ul>

             </div>`
})

export class ErrorPopupComponent implements OnChanges {

  @Input()
  message: string = '';

  @Input()
  show: boolean = false;

  @Input()
  showStopNow: boolean = false;

  show2 = false;

  private timeout: any;

  private lastRand: any;
  private lastRand2: any;

  messageArray: Array<string> = [];
  messageArraySet: boolean = false;
  messageParsed: string = '';

  ngOnChanges(changes: {[show: string]: SimpleChange}) {
    if (changes['show'].currentValue !== this.lastRand) {
      if (changes['show'].currentValue !== 0) {
        this.startShowing();
      }
      this.parseError(this.message);
    }

    if (typeof changes['stopShowing'] !== 'undefined') {
      if (changes['stopShowing'].currentValue !== this.lastRand2) {
        this.stopShowingNow();
      }
    }
  }


  startShowing() {
      this.lastRand = this.show;
      this.show2 = true;
      this.timeout = setTimeout(() => {
        this.show2 = false;
        this.messageArray = [];
        this.messageArraySet = false;
      }, 5000);
  }

  stopShowingNow() {
    this.lastRand2 = this.showStopNow;
    clearTimeout(this.timeout);
    this.show2 = false;
  }


  parseError(err) {
    if (typeof err === 'object') {
      if (this.messageArraySet === false) {
        for (let key in err) {
          if (err.hasOwnProperty(key)) {
            for (let val of err[key]) {
              this.messageArray.push(val);
            }
          }
        }
        this.messageArraySet = true;
      }
    }else {
      this.messageParsed = err;
    }
  }

}
