import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ChatService } from './../services/inGame/chatService';
import { SocketService } from './../services/socketService';
import { ErrorService } from './../services/inGame/errorService';

import { RoomDetail } from './../responses/inGame/chatResponse';

import { findIndex } from 'lodash';

@Component({
  selector: 'as-chat',
  templateUrl: 'app/helpers/chat.html',
  styles: [`
    .barScrollWrapper{
      position: relative;
      overflow: hidden;
      height: 100%;
      margin-top:15px;
    }
    @keyframes blinkAnimation {
      50% {
        background-color: #2a9fd6;
      }
    }
    @-webkit-keyframes blinkAnimation {
      50% {
        background-color: #222222;
      }
    }
    .blinkTab {
      animation: blinkAnimation 3s step-start 0s infinite;
      -webkit-animation: blinkAnimation 3s step-start 0s infinite;
      border-radius: 4px 4px 0 0;
      margin-right: 2px;
    }

    `]
})


export class ChatComponent implements OnInit, OnDestroy {

  rooms: Array<RoomDetail>;
  messageModel: string;
  private subs = [];

  isMobile: boolean = false;
  isBodyShow: boolean = false;

  newMessageStatus: boolean = false;
  newMessageRoom: number;
  activeTab: number = 0;

  @Input()
  maxHeight: number = 100;

  constructor(private chatService: ChatService, private socketService: SocketService,
              private errorService: ErrorService) { }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 768 ? true : false;
    this.isBodyShow = this.isMobile ? false : true;
    this.loadRoomsList();
    this.watchChatMessage();
  }

  showBody() {
    if (this.isMobile) {
      this.isBodyShow = this.isBodyShow ? false : true;
    }
  }

  private loadRoomsList() {
    const sub = this.chatService.getChatRoomList()
                    .subscribe(
                      success => {
                        this.rooms = success.data;
                        this.rooms.map(room => {
                          this.socketService.joinChatRoom(room.roomID);
                        });
                      }
                    );
    this.subs.push(sub);
  }

  private watchChatMessage() {
    const sub = this.socketService.$newChatMessage
                    .subscribe(
                      message => {
                        this.newMessageRoom = message.data.room;
                        this.newMessageStatus = true;
                        const index = findIndex(this.rooms, { 'roomID': message.data.room });
                        if (index !== -1) {
                          this.rooms[index].messages.push(message.data);
                        }
                      }
                    );
    this.subs.push(sub);
  }

  sendMessage(roomID) {
    const sub = this.chatService.sendMessage(this.messageModel, roomID)
                    .subscribe(
                      success => {
                        this.messageModel = '';
                      },
                      error => this.errorService.setError(error)
                    );
    this.subs.push(sub);
  }




  ngOnDestroy() {
    this.subs.map(sub => {
      sub.unsubscribe();
    });
  }


}
