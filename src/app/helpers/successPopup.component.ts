import {Component , Input , OnChanges , SimpleChange} from '@angular/core';


@Component({
  selector: 'as-successPopup',
  template: `<div [hidden]="!show2" class="alert alert-success">{{message}}</div>`
})

export class SuccessPopupComponent implements OnChanges {

  @Input()
  message: string = '';

  @Input()
  show: boolean = false;

  @Input()
  showStopNow: boolean = false;

  show2 = false;

  private _timeout: any;

  private _lastRand: any;
  private _lastRand2: any;

  ngOnChanges(changes: {[show: string]: SimpleChange}) {
    if (changes['show'].currentValue !== this._lastRand) {
      if (changes['show'].currentValue !== 0) {
        this.startShowing();
      }
    }

    if (typeof changes['stopShowing'] !== 'undefined') {
      if (changes['stopShowing'].currentValue !== this._lastRand2) {
        this.stopShowingNow();
      }
    }
  }


  startShowing() {
      this._lastRand = this.show;
      this.show2 = true;
      this._timeout = setTimeout(() => {
        this.show2 = false;
      }, 5000);
  }

  stopShowingNow() {
    this._lastRand2 = this.showStopNow;
    clearTimeout(this._timeout);
    this.show2 = false;
  }




}
