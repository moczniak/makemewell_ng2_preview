import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'as-inGame-paginate',
  template: `
    <ul class="pagination  pagination-sm" [hidden]="!currentPage">
      <li [class.disabled]="currentPage - 1 <= 0"><a class="pointer" (click)="selectPage(currentPage - 1)">&laquo;</a></li>
      <li *ngIf="currentPage - 2 > 0"><a class="pointer" (click)="selectPage(currentPage - 2)">{{currentPage - 2}}</a></li>
      <li *ngIf="currentPage - 1 > 0"><a class="pointer" (click)="selectPage(currentPage - 1)">{{currentPage - 1}}</a></li>
      <li class="active"><a class="pointer" (click)="selectPage(currentPage)">{{currentPage}}</a></li>
      <li *ngIf="currentPage + 1 <= lastPage"><a class="pointer" (click)="selectPage(currentPage + 1)">{{currentPage + 1}}</a></li>

      <li *ngIf="lastPage - 1 > currentPage + 1" class="disabled"><a class="pointer" (click)="selectPage(0)">...</a></li>


      <li *ngIf="lastPage - 1 > currentPage + 1"><a class="pointer" (click)="selectPage(lastPage - 1)">{{lastPage - 1}}</a></li>
      <li *ngIf="lastPage > currentPage + 1"><a class="pointer" (click)="selectPage(lastPage)">{{lastPage}}</a></li>
      <li [class.disabled]="currentPage + 1 > lastPage"><a class="pointer" (click)="selectPage(currentPage + 1)">&raquo;</a></li>
    </ul>
  `
})


export class PaginateComponent {

  @Output()
  onSelectPage = new EventEmitter<number>();

  @Input()
  currentPage: number;

  @Input()
  lastPage: number;

  selectPage(page: number) {
    if (page > 0  && page <= this.lastPage) {
      this.onSelectPage.emit(page);
    }
  }
}
