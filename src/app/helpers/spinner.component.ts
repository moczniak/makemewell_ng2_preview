import {Component, OnInit} from '@angular/core';

import {SpinnerService} from './../services/spinnerService';

@Component({
  selector: 'as-spinner',
  template: `<div class="overlay" [hidden]="!show">
                <i id="loadingSpinner" class="loadingSpinner main-spinner fa fa-spin fa-spinner"></i>
             </div>`
})
export class SpinnerComponent implements OnInit {

  private timeout: any;
  show: boolean = false;

  constructor(private spinnerService: SpinnerService) {
    this.spinnerService.isSpinning$
            .subscribe(value => {
              value === true ? this.startShowing() : this.stopShowing();
            });
  }

  ngOnInit() {
    this.centerSpinner();
    window.onresize = () => {
        this.centerSpinner();
    };
  }


  centerSpinner() {
    this.show = true;
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let spinner = document.getElementById('loadingSpinner');
    let spinnerWidth = spinner.offsetWidth;
    let spinnerHeight = spinner.offsetHeight;

    spinner.style.position = 'absolute';
    spinner.style.top = ((windowHeight - spinnerHeight) / 2) + 'px';
    spinner.style.left = ((windowWidth - spinnerWidth) / 2) + 'px';
    this.show = false;
  }

  startShowing() {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.show = true;
    }, 500);
  }

  stopShowing() {
    clearTimeout(this.timeout);
    this.show = false;
  }
}
